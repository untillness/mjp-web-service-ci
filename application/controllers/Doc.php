<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Doc extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($token = '0')
	{
		if (!$this->sess->isAuth()) {
			return redirect(base_url('home'));
		}
		$data['css'] = "";
		$data['title'] = "EcoMercy | Token";
		$email = $this->sess->get('data')->data->email;
		$data['token'] = $this->User_model->get_token($email);
		$this->load->view('template/top', $data);
		$this->load->view('template/header');
		$this->load->view('doc/token', $data);
		$this->load->view('template/bottom');
	}

	public function product()
	{
		$data['css'] = "";
		$data['title'] = "EcoMercy | Product";
		$this->load->view('template/top', $data);
		$this->load->view('template/header');
		$this->load->view('doc/index');
		$this->load->view('template/bottom');
	}
}
