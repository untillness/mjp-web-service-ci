<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $table = 'products';

    function getProduk($id, $idUser)
    {
        $this->db->where('id_user', $idUser);
        if ($id == null) {
            $product = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id', $id);
            $product = $this->db->get($this->table)->row();
        }
        return $product;
    }

    public function insertProduk($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function cekId($id, $idUser)
    {
        $data = $this->db->get_where($this->table, ['id' => $id, 'id_user' => $idUser]);
        return $data->num_rows();
    }

    public function updateProduct($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
    }

    public function deleteProduct($id, $idUser)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}
