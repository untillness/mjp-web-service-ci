<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $table = 'users';

    public function get_by_email_count($email)
    {
        $data = $this->db->get_where($this->table, ['email' => $email]);
        return $data->num_rows();
    }

    public function create($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function get_by_email($email)
    {
        $data = $this->db->get_where($this->table, ['email' => $email]);
        return $data->row();
    }

    public function update($id, $token)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, [
            'token' => $token
        ]);
    }

    public function get_token($email)
    {
        $data = $this->get_by_email($email);
        return $data->token;
    }

    public function check_token($token)
    {
        $get = $this->db->get_where($this->table, ['token' => $token])->num_rows();
        return $get > 0 ? true : false;
    }
}
